trigger CaseTrigger on Case (before insert,before update) {

    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            CaseTriggerHandler.checkChildStatus(Trigger.new);
        }
    }
}