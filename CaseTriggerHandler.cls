public class CaseTriggerHandler {
    
    public static void checkChildStatus(List<Case> newCaseList){
                
        List<Case> closedCaseList = new List<Case>();
        for(Case caseObject : newCaseList){
            if(caseObject.Status == 'Closed'){
                closedCaseList.add(caseObject);
            }
        }
        List<Case> childCaseList = [SELECT Id,ParentId,Status FROM Case WHERE ParentId IN : closedCaseList];
        List<Id> idList = new List<Id>();
        Map<Id,Case> mapCase = new Map<Id,Case>();
        for(Case parent : closedCaseList){
            mapCase.put(parent.Id,parent);
        }
        
        for(Case child : childCaseList){
            if(child.Status != 'Closed'){
                //child.ParentId.addError('Child account is not closed');
                mapCase.get(child.ParentId).addError('Child account is not closed');
            }
        }
    }

}