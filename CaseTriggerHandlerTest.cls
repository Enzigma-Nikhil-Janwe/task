@isTest
public class CaseTriggerHandlerTest {
    @isTest
    static void testCheckChildStatus() {
        // Create parent cases
        List<Case> parentCases = new List<Case>();
        parentCases.add(new Case(Subject = 'Parent Case ' , Status = 'New'));
        insert parentCases;
        
        // Create child cases
        List<Case> childCases = new List<Case>();
        for (Integer i = 0; i < 20; i++) { //Child Bulkification
            childCases.add(new Case(Subject = 'Child Case ' + i, Status = 'New', ParentId = parentCases[0].Id));
        }
        insert childCases;
        
        // Close parent cases
        List<Case> closedParentCases = new List<Case>();
        // for (Integer i = 0; i < 100; i++) {
        parentCases[0].Status = 'Closed';
        //  closedParentCases.add(parentCases[i]);
        //}
        try{
            update parentCases; //trigger is hit
        }catch(Exception e){ 
            System.debug('Message:'+e.getMessage());
            System.assertEquals(true, e.getMessage().contains('Child account is not closed'));
        }
    }
}